-- Active: 1709562234048@@127.0.0.1@3306@ecom-back
-- Insert categories
INSERT INTO CATEGORY (ID, NAME) VALUES
(1, 'Makeup'),
(2, 'Skincare'),
(3, 'Fragrances');

-- Insert products
INSERT INTO PRODUCT (ID, NAME, STOCK, PRICE, DESCRIPTION, PHOTO, CAPACITY) VALUES
(1, 'Mascara', true, 10.99, 'Lengthening and volumizing mascara', 'mascara.jpg', 10),
(2, 'Moisturizer', true, 19.99, 'Hydrating moisturizer for dry skin', 'moisturizer.jpg', 200),
(3, 'Perfume', true, 49.99, 'Floral perfume for women', 'perfume.jpg', 50),
(4, 'Eyeshadow', true, 14.99, 'Neutral eyeshadow palette', 'eyeshadow.jpg', 12),
(5, 'Lipstick', true, 12.99, 'Matte lipstick in red', 'lipstick.jpg', 10);

-- Insert product categories
INSERT INTO CATEGORY_PRODUCT (CATEGORY_ID, PRODUCT_ID) VALUES
(1, 1),
(1, 4),
(1, 5),
(2, 2),
(3, 3);

-- Insert users
INSERT INTO USER (ID, EMAIL, PASSWORD, ROLE) VALUES
(1, 'user1@example.com', 'password1', 'USER'),
(2, 'user2@example.com', 'password2', 'USER'),
(3, 'admin@example.com', 'password3', 'ADMIN');

-- Insert purchase orders
INSERT INTO PURCHASE_ORDER (ID, DATE, TOTAL_PRICE, USER_ID) VALUES
(1, '2022-01-01 10:00:00', 50.00, 1),
(2, '2022-01-05 12:00:00', 30.00, 2),
(3, '2022-01-10 14:00:00', 80.00, 1);

-- Insert order lines
INSERT INTO ORDER_LINE (ID, QUANTITY, CURRENT_PRICE, PURCHASE_ORDERS_ID) VALUES
(1, 2, 25.00, 1),
(2, 1, 30.00, 2),
(3, 3, 20.00, 3);



-- Insert reviews
INSERT INTO REVIEW (ID, NOTE, COMMENT, DATE, USER_ID, PRODUCT_ID) VALUES
(1, 5, 'Great product!', '2022-01-02 10:00:00', 1, 1),
(2, 4, 'Good but not great', '2022-01-06 12:00:00', 2, 2),
(3, 5, 'Love this perfume!', '2022-01-11 14:00:00', 1, 3);

-- Insert user products
INSERT INTO USER_PRODUCT (USER_ID, PRODUCT_ID) VALUES
(1, 1),
(1, 4),
(2, 2),
(2, 5);