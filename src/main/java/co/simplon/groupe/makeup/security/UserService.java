package co.simplon.groupe.makeup.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import co.simplon.groupe.makeup.repositories.UserRepository;




@Service
public class UserService implements UserDetailsService{

    @Autowired
    private UserRepository repo;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repo.findByEmail(username)
        .orElseThrow(() -> new UsernameNotFoundException("user does not exist"));
    }

}
