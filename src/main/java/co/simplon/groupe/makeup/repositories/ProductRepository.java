package co.simplon.groupe.makeup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.groupe.makeup.entities.Product;

@Repository
public interface  ProductRepository extends JpaRepository<Product,Integer> {
    
   

}