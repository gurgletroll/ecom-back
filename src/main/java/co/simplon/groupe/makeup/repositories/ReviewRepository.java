package co.simplon.groupe.makeup.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.groupe.makeup.entities.Review;

@Repository
public interface ReviewRepository extends JpaRepository<Review,Integer> {

}
