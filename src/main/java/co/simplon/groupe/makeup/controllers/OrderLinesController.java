package co.simplon.groupe.makeup.controllers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.groupe.makeup.entities.OrderLine;
import co.simplon.groupe.makeup.repositories.OrderLineRepository;

@RestController
@RequestMapping("/api/orderlines")
public class OrderLinesController {

    @Autowired
    private OrderLineRepository repo;

    @GetMapping
    public List<OrderLine> getAllOrderLines() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public OrderLine getOrderLineById(@PathVariable Integer id) {
        return repo.findById(id).orElse(null);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OrderLine createOrderLine(@RequestBody OrderLine orderLine) {
        return repo.save(orderLine);
    }

    @PutMapping("/{id}")
    public OrderLine updateOrderLine(@PathVariable Integer id, @RequestBody OrderLine orderLine) {
        OrderLine existingOrderLine = repo.findById(id).orElse(null);
        if (existingOrderLine != null) {
            existingOrderLine.setQuantity(orderLine.getQuantity());
            existingOrderLine.setCurrentPrice(orderLine.getCurrentPrice());
            existingOrderLine.setProducts(orderLine.getProducts());
            existingOrderLine.setPurchaseOrders(orderLine.getPurchaseOrders());
            return repo.save(existingOrderLine);
        }
        return null;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderLine(@PathVariable Integer id) {
        repo.deleteById(id);
    }
}
