package co.simplon.groupe.makeup.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.groupe.makeup.entities.Category;
import co.simplon.groupe.makeup.repositories.CategoryRepository;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@RestController
@RequestMapping("api/category")

public class CategoryController {


    @Autowired
    private CategoryRepository repo;

    @GetMapping
    public List<Category> getAllCategories(){
      return  repo.findAll();
        
    }

    @GetMapping("/{id}")
    public Category getCategorybyId(@PathVariable Integer id)   {
        return repo.findById(id).orElse(null);    
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Category createCategory(@RequestBody Category category){
        return repo.save(category);
        
    }


   @PutMapping("/{id}")
    public Category updtadeCategory(@PathVariable Integer id, @RequestBody Category category){
    
    Category existinCategory = repo.findById(id).orElse(null);

    if (existinCategory!=null){

        existinCategory.setName(category.getName());
        existinCategory.setProducts(category.getProducts());
    }


        return null;
    } 


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCategory(@PathVariable Integer id){

        repo.deleteById(id);
    }



} 
 