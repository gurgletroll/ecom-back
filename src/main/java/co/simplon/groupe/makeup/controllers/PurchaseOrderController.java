package co.simplon.groupe.makeup.controllers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.groupe.makeup.entities.PurchaseOrder;
import co.simplon.groupe.makeup.repositories.PurchaseOrderRepository;

@RestController
@RequestMapping("/api/order")

public class PurchaseOrderController {


    @Autowired
    private PurchaseOrderRepository repo;

    @GetMapping
    public List<PurchaseOrder> getAllPurchaseOrder() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public PurchaseOrder getPurchaseOrderById(@PathVariable Integer id) {
        return repo.findById(id).orElse(null);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PurchaseOrder createPurchaseOrder(@RequestBody PurchaseOrder purchaseOrder) {
        return repo.save(purchaseOrder);
    }

   @PutMapping("/{id}")
    public PurchaseOrder updatePurchaseOrder(@PathVariable Integer id, @RequestBody PurchaseOrder purchaseOrder) {
        PurchaseOrder existingPurchaseOrder = repo.findById(id).orElse(null);
        if (existingPurchaseOrder != null) {
            
            existingPurchaseOrder.setDate(purchaseOrder.getDate());
            
            existingPurchaseOrder.setTotalPrice(purchaseOrder.getTotalPrice());

            existingPurchaseOrder.setOrderLines(purchaseOrder.getOrderLines());

            existingPurchaseOrder.setUser(purchaseOrder.getUser());


        }
        return null;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderLine(@PathVariable Integer id) {
        repo.deleteById(id);
    }}

