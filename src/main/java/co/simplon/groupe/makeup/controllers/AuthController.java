package co.simplon.groupe.makeup.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.groupe.makeup.entities.User;
import co.simplon.groupe.makeup.repositories.UserRepository;
import jakarta.validation.Valid;

@RestController
public class AuthController {

    @Autowired
    private PasswordEncoder hasher;
    @Autowired
    private UserRepository repo;

    @PostMapping("api/user")
    public User register(@Valid @RequestBody User user) {

        if (repo.findByEmail(user.getEmail()).isPresent()) {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }
        String hash = hasher.encode(user.getPassword());
        user.setPassword(hash);
        user.setRole("ROLE_USER");
        repo.save(user);
        return user;
    }

}
